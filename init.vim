"basic settings

set completeopt=preview,nomenu,noinsert
set cursorline
set termguicolors
set colorcolumn=150
set mouse=a
set nu
set autoindent
set smartindent
set laststatus=2
syntax on
syntax enable
set smartindent
filetype plugin on
filetype plugin indent on
filetype indent on
set noerrorbells
set background=dark
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set relativenumber
""----------------------------------------vim plug---------------------------------------vim-----------------awesome----
call plug#begin('~/AppData/Local/nvim/plugged')
Plug 'mhartington/nvim-typescript'
Plug 'editorconfig/editorconfig-vim'
Plug 'joshdick/onedark.vim'
Plug 'sainnhe/gruvbox-material'
Plug 'sainnhe/edge'
Plug 'ajmwagar/vim-deus'
Plug 'tpope/vim-sleuth'
Plug 'gruvbox-community/gruvbox'
Plug 'valloric/youcompleteme'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mbbill/undotree'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf', {'do': {->fzf#install()}}
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-fugitive'
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'
Plug 'pangloss/vim-javascript'
Plug 'nvim-lua/completion-nvim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'rmagatti/goto-preview'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'rust-lang/rust.vim'
Plug 'spinks/vim-leader-guide'
Plug 'kdheepak/tabline.nvim'
call plug#end()
""settings for colorscheme and vim tabline
let g:airline_powerline_fonts = 1
let g:rainbow_active = 1
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_colors=216
let g:gruvbox_italic=1
let g:gruvbox_bold=1
colorscheme gruvbox
if has('g:neovide_cursor_vfx_mode')
		let g:neovide_cursor_vfx_mode = "pixiedust"
		let g:neovide_cursor_vfx_opacity=100
endif

if has('fvim')
		map <F11> :FVimToggleFullScreen<CR>
endif
map <space>g :YcmCompleter GoTo <CR>
map <space>r :YcmCompleter RefactorRename
map <space>d :YcmCompleter GoToDeclaration <CR>
inoremap <silent> <S-Insert> <C-R>
map tt :Telescope <CR>
map tu :UndotreeToggle <CR>
map te :Vexplore <CR>
map tn :NERDTree <CR>
map <C-v> :source $VIMRUNTIME/mswin.vim <CR>
nnoremap <space>v+ :vertical resize +5<CR>
nnoremap <space>v+ :vertical resize +5<CR>
nnoremap <space>h- :resize -5<CR>
nnoremap <space>h+ :resize +5<CR>
map <tab>+ :tabnew <CR>
map <C-Tab> gt
map `i :PlugInstall <CR>
map `s :source % <CR>
map <F5> :w ! python
map `u :PlugUpdate <CR>
map `pg :PlugUpgrade <CR>
map `c :PlugClean <CR>
tnoremap <C-q> <C-\><C-n> 
map ; :
map f~ :FZF~ <CR>
map f :FZF <CR>
map tf :Telescope find_files <CR>
map tfl :Telescope file_browser<CR>
map tch :Telescope command_history<CR>
map <C-s> :w <CR>
map <C-q> :wq <CR>
map <C-n> :tabnew <CR>
map <tab> <C-w><C-w>
nnoremap <silent> K <cmd>lua require('lspsaga.hover').render_hover_doc()<CR>
vnoremap <silent> <C> :<c-u>LeaderGuideVisual '<Space>'<CR>

"getting some lua settings
lua require('lua')
