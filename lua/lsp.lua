require'lspconfig'.pylsp.setup{}
require'lspconfig'.pylsp.setup{on_attach=require'completion'.on_attach}
require'lspconfig'.tsserver.setup{}
require'lspconfig'.tsserver.setup{on_attach=require'completion'.on_attach}
require'nvim-treesitter.configs'.setup {
  ensure_installed = "python", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ignore_install = { "javascript" }, -- List of parsers to ignore installing
  highlight = {
    enable = true,              -- false will disable the whole extension
    disable = {},  -- list of language that will be disabled
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
local ts_utils = require 'nvim-treesitter.ts_utils'
require('telescope.builtin').find_files({layout_strategy='vertical',layout_config={width=0.5}})
